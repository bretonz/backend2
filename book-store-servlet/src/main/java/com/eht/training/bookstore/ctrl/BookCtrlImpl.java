/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.service.BookService;
import com.eht.training.bookstore.vo.BookVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping( value = "/book")
public class BookCtrlImpl implements BookCtrl {

    @RequestMapping(
            method = RequestMethod.POST, 
            consumes = "application/json"
    )
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addBook(
            @RequestBody BookVO bookVO,
            @RequestHeader(value = "EHT-training-Token")String token) {
        this.bookService.addBook(bookVO, "123");

    }
    
    @Autowired
    private BookService bookService;
}
