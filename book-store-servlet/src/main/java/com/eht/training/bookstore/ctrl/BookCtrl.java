/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.vo.BookVO;

/**
 *
 * @author mbret
 */
public interface BookCtrl {
    void addBook(BookVO bookVO, String token);
}
