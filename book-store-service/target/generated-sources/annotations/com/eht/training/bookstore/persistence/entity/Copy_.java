package com.eht.training.bookstore.persistence.entity;

import com.eht.training.bookstore.persistence.entity.Book;
import com.eht.training.bookstore.persistence.entity.UserCopy;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-09T19:40:38")
@StaticMetamodel(Copy.class)
public class Copy_ { 

    public static volatile SingularAttribute<Copy, Integer> copyId;
    public static volatile SingularAttribute<Copy, String> statusCopy;
    public static volatile SingularAttribute<Copy, Book> isbn;
    public static volatile SingularAttribute<Copy, String> ubication;
    public static volatile SingularAttribute<Copy, UserCopy> loanId;

}