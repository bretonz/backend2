package com.eht.training.bookstore.persistence.entity;

import com.eht.training.bookstore.persistence.entity.Role;
import com.eht.training.bookstore.persistence.entity.Session;
import com.eht.training.bookstore.persistence.entity.UserCopy;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-09T19:40:38")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, String> photoUrl;
    public static volatile SingularAttribute<User, String> password;
    public static volatile CollectionAttribute<User, Role> roleCollection;
    public static volatile SingularAttribute<User, Long> phonenumber;
    public static volatile CollectionAttribute<User, Session> sessionCollection;
    public static volatile SingularAttribute<User, Integer> userId;
    public static volatile SingularAttribute<User, String> email;
    public static volatile SingularAttribute<User, UserCopy> loanId;
    public static volatile SingularAttribute<User, String> username;
    public static volatile SingularAttribute<User, String> lastname;

}