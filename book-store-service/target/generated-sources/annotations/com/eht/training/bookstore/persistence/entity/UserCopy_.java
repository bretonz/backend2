package com.eht.training.bookstore.persistence.entity;

import com.eht.training.bookstore.persistence.entity.Copy;
import com.eht.training.bookstore.persistence.entity.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-09T19:40:38")
@StaticMetamodel(UserCopy.class)
public class UserCopy_ { 

    public static volatile SingularAttribute<UserCopy, Date> returnDate;
    public static volatile SingularAttribute<UserCopy, String> returnComment;
    public static volatile CollectionAttribute<UserCopy, User> userCollection;
    public static volatile SingularAttribute<UserCopy, Date> requestDate;
    public static volatile SingularAttribute<UserCopy, Date> maxreturnDate;
    public static volatile SingularAttribute<UserCopy, Integer> loanId;
    public static volatile CollectionAttribute<UserCopy, Copy> copyCollection;

}