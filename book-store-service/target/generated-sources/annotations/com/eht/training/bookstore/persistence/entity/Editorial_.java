package com.eht.training.bookstore.persistence.entity;

import com.eht.training.bookstore.persistence.entity.Book;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-09T19:40:38")
@StaticMetamodel(Editorial.class)
public class Editorial_ { 

    public static volatile SingularAttribute<Editorial, String> editorial;
    public static volatile SingularAttribute<Editorial, Integer> editorialId;
    public static volatile CollectionAttribute<Editorial, Book> bookCollection;

}