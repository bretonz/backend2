package com.eht.training.bookstore.persistence.entity;

import com.eht.training.bookstore.persistence.entity.Author;
import com.eht.training.bookstore.persistence.entity.Category;
import com.eht.training.bookstore.persistence.entity.Copy;
import com.eht.training.bookstore.persistence.entity.Editorial;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-09T19:40:38")
@StaticMetamodel(Book.class)
public class Book_ { 

    public static volatile SingularAttribute<Book, Editorial> editorialId;
    public static volatile CollectionAttribute<Book, Author> authorCollection;
    public static volatile SingularAttribute<Book, Integer> isbn;
    public static volatile SingularAttribute<Book, String> title;
    public static volatile SingularAttribute<Book, Category> categoryId;
    public static volatile CollectionAttribute<Book, Copy> copyCollection;

}