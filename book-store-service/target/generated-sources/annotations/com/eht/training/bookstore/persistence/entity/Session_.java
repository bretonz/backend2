package com.eht.training.bookstore.persistence.entity;

import com.eht.training.bookstore.persistence.entity.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-09T19:40:38")
@StaticMetamodel(Session.class)
public class Session_ { 

    public static volatile SingularAttribute<Session, User> userId;
    public static volatile SingularAttribute<Session, String> token;

}