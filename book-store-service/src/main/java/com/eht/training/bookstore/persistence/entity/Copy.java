/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fabian
 */
@Entity
@Table(name = "copy")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Copy.findAll", query = "SELECT c FROM Copy c"),
    @NamedQuery(name = "Copy.findByCopyId", query = "SELECT c FROM Copy c WHERE c.copyId = :copyId"),
    @NamedQuery(name = "Copy.findByStatusCopy", query = "SELECT c FROM Copy c WHERE c.statusCopy = :statusCopy"),
    @NamedQuery(name = "Copy.findByUbication", query = "SELECT c FROM Copy c WHERE c.ubication = :ubication")})
public class Copy implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "COPY_ID")
    private Integer copyId;
    @Basic(optional = false)
    @Column(name = "STATUS_COPY")
    private String statusCopy;
    @Column(name = "UBICATION")
    private String ubication;
    @JoinColumn(name = "ISBN", referencedColumnName = "ISBN")
    @ManyToOne(optional = false)
    private Book isbn;
    @JoinColumn(name = "LOAN_ID", referencedColumnName = "LOAN_ID")
    @ManyToOne
    private UserCopy loanId;

    public Copy() {
    }

    public Copy(Integer copyId) {
        this.copyId = copyId;
    }

    public Copy(Integer copyId, String statusCopy) {
        this.copyId = copyId;
        this.statusCopy = statusCopy;
    }

    public Integer getCopyId() {
        return copyId;
    }

    public void setCopyId(Integer copyId) {
        this.copyId = copyId;
    }

    public String getStatusCopy() {
        return statusCopy;
    }

    public void setStatusCopy(String statusCopy) {
        this.statusCopy = statusCopy;
    }

    public String getUbication() {
        return ubication;
    }

    public void setUbication(String ubication) {
        this.ubication = ubication;
    }

    public Book getIsbn() {
        return isbn;
    }

    public void setIsbn(Book isbn) {
        this.isbn = isbn;
    }

    public UserCopy getLoanId() {
        return loanId;
    }

    public void setLoanId(UserCopy loanId) {
        this.loanId = loanId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (copyId != null ? copyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Copy)) {
            return false;
        }
        Copy other = (Copy) object;
        if ((this.copyId == null && other.copyId != null) || (this.copyId != null && !this.copyId.equals(other.copyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.eht.training.bookstore.persistence.entity.Copy[ copyId=" + copyId + " ]";
    }
    
}
