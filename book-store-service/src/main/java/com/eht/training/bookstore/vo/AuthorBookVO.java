/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.vo;

/**
 *
 * @author fabian
 */
public class AuthorBookVO {
     private AuthorVO author;
    private BookVO isbn;

    public AuthorVO getAuthor() {
        return author;
    }

    public void setAuthor(AuthorVO author) {
        this.author = author;
    }

    public BookVO getIsbn() {
        return isbn;
    }

    public void setIsbn(BookVO isbn) {
        this.isbn = isbn;
    }

    
}
