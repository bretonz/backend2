/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CrudDAO<T> implements GenericDAO<T> {
    @Autowired
    @Qualifier("sessionFactory")
    SessionFactory session;
    Class<T> domainClass;
    
    private Session getSession(){
        return this.session.getCurrentSession();
    }
    
    private Class<T> getDomainClass(){
        if(domainClass == null){
            ParameterizedType domainType = (ParameterizedType) getClass().getGenericSuperclass();
            this.domainClass = (Class<T>) domainType.getActualTypeArguments()[0];
        }
        
        return this.domainClass;
    }
    
    private String getDomainName(){
    return getDomainClass().getName();
    }
    
    @Override
    public Collection<T> getAll() {
        return getSession().createQuery(" FROM "+getDomainName()).list();
    }

    @Override
    public void create(T entity) {
        getSession().save(entity);
        getSession().flush();
    }

    @Override
    public T read(Serializable id) {
        return (T) getSession().get(getDomainClass(), id);
    }

    @Override
    public void update(T entity) {
        getSession().update(entity);
        getSession().flush();
    }

    @Override
    public void delete(Serializable id) {
        T entity = read(id);
        
        if(entity != null){
            getSession().delete(entity);
            getSession().flush();
        }
    }
    
}
