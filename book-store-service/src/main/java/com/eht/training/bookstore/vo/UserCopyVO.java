/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.vo;

import java.util.Date;

/**
 *
 * @author fabian
 */
public class UserCopyVO {
    private int loanId;
    private Date requestDate;
    private Date responseDate;
    private Date maxReturnDate;
    private String returnComment;

    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public Date getMaxReturnDate() {
        return maxReturnDate;
    }

    public void setMaxReturnDate(Date maxReturnDate) {
        this.maxReturnDate = maxReturnDate;
    }

    public String getReturnComment() {
        return returnComment;
    }

    public void setReturnComment(String returnComment) {
        this.returnComment = returnComment;
    }
    
    
}
