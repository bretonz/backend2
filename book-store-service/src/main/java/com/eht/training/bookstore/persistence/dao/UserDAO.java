/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.dao;

import com.eht.training.bookstore.persistence.entity.User;

/**
 *
 * @author mbret
 */
public interface UserDAO extends GenericDAO<User>{
    
}
