/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.dao;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mbret
 */
public interface GenericDAO<T extends Object> {
    Collection<T> getAll();
    void create(T entity);
    T read(Serializable id);
    void update(T entity);
    void delete(Serializable id);
}
