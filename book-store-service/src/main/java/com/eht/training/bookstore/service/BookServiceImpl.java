/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.service;

import com.eht.training.bookstore.persistence.dao.BookDAO;
import com.eht.training.bookstore.persistence.dao.CategoryDAO;
import com.eht.training.bookstore.persistence.dao.EditorialDAO;
import com.eht.training.bookstore.persistence.entity.Author;
import com.eht.training.bookstore.persistence.entity.Book;
import com.eht.training.bookstore.persistence.entity.Category;
import com.eht.training.bookstore.persistence.entity.Editorial;
import com.eht.training.bookstore.vo.AuthorVO;
import com.eht.training.bookstore.vo.BookVO;
import java.util.ArrayList;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

    
    public void addBook(BookVO bookVO, String token) {
        
        if(bookVO.getIsbn() <=0) throw new IllegalArgumentException("Invalid isbn");
            if(bookVO.getTitle().length() >20) throw new IllegalArgumentException("Invalid title");
        Category category = this.categoryDao.read(bookVO.getCategory().getCategory_Id());
            if (category == null) throw new NullPointerException("Category not found");
        Editorial editorial = this.editorialDAO.read(bookVO.getEditorial().getEditorial_Id());
            if (editorial == null ) throw new NullPointerException("Editorial not found");
        
        Book book = new Book();  
        book.setIsbn(bookVO.getIsbn());
        book.setTitle(bookVO.getTitle());
        book.setCategoryId(category);
        book.setEditorialId(editorial);
        
         
        Collection<Author> authors =  new ArrayList<>();
        for (AuthorVO authorVO : bookVO.getAuthors()) {
            Author author = new Author();
            author.setAuthor(authorVO.getAuthor());
            author.setLastname(authorVO.getLastName());
            
            authors.add(author);
        }
        
        book.setAuthorCollection(authors);
          
        this.bookDao.create(book);
        
    }
    
    
    @Autowired
    private BookDAO bookDao; 
    private CategoryDAO categoryDao;
    private EditorialDAO editorialDAO;
}
