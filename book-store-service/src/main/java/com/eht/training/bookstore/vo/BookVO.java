/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.vo;

import java.util.List;

/**
 *
 * @author fabian
 */
public class BookVO {
     private int isbn;
    private String title;
    private CategoryVO category;
    private EditorialVO editorial;
    private List<AuthorVO> authors;

    public List<AuthorVO> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorVO> authors) {
        this.authors = authors;
    }
    
    

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CategoryVO getCategory() {
        return category;
    }

    public void setCategory(CategoryVO category) {
        this.category = category;
    }

    public EditorialVO getEditorial() {
        return editorial;
    }

    public void setEditorial(EditorialVO editorial) {
        this.editorial = editorial;
    }
    
}
