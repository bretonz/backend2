/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author fabian
 */
@Entity
@Table(name = "user_copy")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserCopy.findAll", query = "SELECT u FROM UserCopy u"),
    @NamedQuery(name = "UserCopy.findByLoanId", query = "SELECT u FROM UserCopy u WHERE u.loanId = :loanId"),
    @NamedQuery(name = "UserCopy.findByRequestDate", query = "SELECT u FROM UserCopy u WHERE u.requestDate = :requestDate"),
    @NamedQuery(name = "UserCopy.findByReturnDate", query = "SELECT u FROM UserCopy u WHERE u.returnDate = :returnDate"),
    @NamedQuery(name = "UserCopy.findByMaxreturnDate", query = "SELECT u FROM UserCopy u WHERE u.maxreturnDate = :maxreturnDate")})
public class UserCopy implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "LOAN_ID")
    private Integer loanId;
    @Basic(optional = false)
    @Column(name = "REQUEST_DATE")
    @Temporal(TemporalType.DATE)
    private Date requestDate;
    @Basic(optional = false)
    @Column(name = "RETURN_DATE")
    @Temporal(TemporalType.DATE)
    private Date returnDate;
    @Basic(optional = false)
    @Column(name = "MAXRETURN_DATE")
    @Temporal(TemporalType.DATE)
    private Date maxreturnDate;
    @Lob
    @Column(name = "RETURN_COMMENT")
    private String returnComment;
    @OneToMany(mappedBy = "loanId")
    private Collection<Copy> copyCollection;
    @OneToMany(mappedBy = "loanId")
    private Collection<User> userCollection;

    public UserCopy() {
    }

    public UserCopy(Integer loanId) {
        this.loanId = loanId;
    }

    public UserCopy(Integer loanId, Date requestDate, Date returnDate, Date maxreturnDate) {
        this.loanId = loanId;
        this.requestDate = requestDate;
        this.returnDate = returnDate;
        this.maxreturnDate = maxreturnDate;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Date getMaxreturnDate() {
        return maxreturnDate;
    }

    public void setMaxreturnDate(Date maxreturnDate) {
        this.maxreturnDate = maxreturnDate;
    }

    public String getReturnComment() {
        return returnComment;
    }

    public void setReturnComment(String returnComment) {
        this.returnComment = returnComment;
    }

    @XmlTransient
    public Collection<Copy> getCopyCollection() {
        return copyCollection;
    }

    public void setCopyCollection(Collection<Copy> copyCollection) {
        this.copyCollection = copyCollection;
    }

    @XmlTransient
    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loanId != null ? loanId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserCopy)) {
            return false;
        }
        UserCopy other = (UserCopy) object;
        if ((this.loanId == null && other.loanId != null) || (this.loanId != null && !this.loanId.equals(other.loanId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.eht.training.bookstore.persistence.entity.UserCopy[ loanId=" + loanId + " ]";
    }
    
}
