/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.dao;

import com.eht.training.bookstore.persistence.entity.Category;

/**
 *
 * @author mbret
 */
public interface CategoryDAO extends GenericDAO<Category>{
    
}
